package com.example.series.test;
import java.util.Random;
import java.util.Scanner;

/**
 * @author Hegedűs Viktor
 * @version 1.1
 * @since 2018.11.20.
 */

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Dual All-Integer algorithm\n");
        openApplication();
    }

    /**
     * openApplication() method is a while loop
     * options:
     *      - quit the program
     *      - run the algorithm -> show only the final solution
     *      - run the algorithm -> show every step
     *      - show main menu
     */

    private static void openApplication() {
        boolean quit = false;
        printMenu();

        while (!quit) {

            System.out.print("Enter a value: ");
            int select;

            if (scanner.hasNextInt()) {
                select = scanner.nextInt();
                scanner.nextLine();

                switch (select) {
                    case 1:
                        System.out.println("\nThe program has stopped.\nQuitting...");
                        quit = true;
                        break;
                    case 2:
                        printEveryStep();
                        System.out.println();
                        printMenu();
                        break;
                    case 3:
                        printFinalSolution();
                        System.out.println();
                        printMenu();
                        break;
                    case 4:
                        System.out.println("\n");
                        printMenu();
                        break;
                    default:
                        System.out.println("\nInvalid value. Enter another number!\n");
                        break;
                }
            } else {
                System.out.println("\nInput is not a number, try again!");
                scanner.next();
            }
        }
    }

    /**
     * Main menu
     */

    private static void printMenu() {
        System.out.println("Press 1 to quit");
        System.out.println("Press 2 to run the algorithm - show every step");
        System.out.println("Press 3 to run the algorithm - show the final solution only");
        System.out.println("Press 4 to open menu");
    }

    /**
     * Shows every step of the algorithm -> prints every iteration
     */

    private static void printEveryStep() {

        int[][] matrix = enterValues();
        int iteration = 0;
        int countConstants = 0;
        int countValues = 0;

        if (hasPositiveConstants(matrix)) {
            System.out.println("\n----------------------------------------");
            System.out.println("----------- OPTIMAL SOLUTION -----------");
            System.out.println("----------------------------------------\n");
            printMatrix("Matrix\n", matrix);
            countConstants++;
        }

        else if (hasPositiveValues(matrix)) {
            System.out.println("\n----------------------------------------");
            System.out.println("--------- NO POSSIBLE SOLUTION ---------");
            System.out.println("----------------------------------------\n");
            printMatrix("Matrix\n", matrix);

            countValues++;
        }

        //System.out.println(hasPositiveValues(matrix));

        while (!hasPositiveConstants(matrix) && !hasPositiveValues(matrix)) {

            System.out.println("\n****************************************");
            System.out.println("************* ITERATION " + iteration++ + " **************");
            System.out.println("****************************************\n");

            //System.out.println("\nHas positive numbers: " + hasPositiveValues(matrix));
            printMatrix("Matrix", matrix);
            int min = findMin(matrix);
            System.out.println("\nMinimum value: " + min + "\n" + "Row: " + findMinRow(matrix)
                    + ", Column: " + findMinColumn(matrix));
            matrix = createNewMatrix(matrix);
            //System.out.println("\nHas positive numbers: " + hasPositiveValues(matrix));
            printMatrix("\nMatrix", matrix);
            int minInVector = findMinInNewVector(matrix);
            System.out.println();
            System.out.println("Minimum value in new vector: " + minInVector);
            System.out.print("Row: " + findMinRowInNewVector(matrix) + ", ");
            System.out.println("Column: " + findMinColumnInNewVector(matrix));
            System.out.println();
            matrix = nextIteration(matrix);
            //System.out.println("\nHas positive numbers: " + hasPositiveValues(matrix));
            printMatrix("Matrix:", matrix);

            if (iteration > 20) {
                System.out.println("\nIt seems the algorithm will not stop\nQuitting...\n");
                break;
            }
        }

        if (hasPositiveConstants(matrix)) {
            if (countConstants == 0) {
                System.out.println("\n----------------------------------------");
                System.out.println("----------- OPTIMAL SOLUTION -----------");
                System.out.println("----------------------------------------\n");
            }
        }

        else if (hasPositiveValues(matrix)) {
            if (countValues == 0) {
                System.out.println("\n----------------------------------------");
                System.out.println("--------- NO POSSIBLE SOLUTION ---------");
                System.out.println("----------------------------------------\n");
            }

        }

        //System.out.print("\033[10;31m______________\033[30m\nIterations: \033[10;30m" + iteration + "\n");
        System.out.print("______________\nIterations: " + iteration + "\n");
        //System.out.println("");
    }

    /**
     * void function: the result of the last iteration only
     * Does not show every step, just the final solution
     */


    private static void printFinalSolution() {

        int[][] matrix = enterValues();
        int iteration = 0;
        int countConstants = 0;
        int countValues = 0;

        if (hasPositiveConstants(matrix)) {
            System.out.println("\n----------------------------------------");
            System.out.println("----------- OPTIMAL SOLUTION -----------");
            System.out.println("----------------------------------------\n");
            printMatrix("Matrix", matrix);
            countConstants++;
        } else if (hasPositiveValues(matrix)) {
            System.out.println("\n----------------------------------------");
            System.out.println("--------- NO POSSIBLE SOLUTION ---------");
            System.out.println("----------------------------------------\n");
            printMatrix("Matrix", matrix);
            countValues++;
        } else {
            printMatrix("\nMatrix at the beginning", matrix);
        }

        //System.out.println(hasPositiveValues(matrix));

        while (!hasPositiveConstants(matrix) && !hasPositiveValues(matrix)) {

            iteration++;
            matrix = createNewMatrix(matrix);
            matrix = nextIteration(matrix);

            if (iteration > 20) {
                System.out.println("\nIt seems the algorithm will not stop\nQuitting...\n");
                break;
            }
        }

        if (hasPositiveConstants(matrix)) {
            if (countConstants == 0) {
                System.out.println("\n----------------------------------------");
                System.out.println("----------- OPTIMAL SOLUTION -----------");
                System.out.println("----------------------------------------\n");
                printMatrix("Final Matrix", matrix);
            }
        } else if (hasPositiveValues(matrix)) {
            if (countValues == 0) {
                System.out.println("\n----------------------------------------");
                System.out.println("--------- NO POSSIBLE SOLUTION ---------");
                System.out.println("----------------------------------------\n");
                printMatrix("Final Matrix", matrix);
            }
        }

        //System.out.print("\033[10;31m______________\033[30m\nIterations: \033[10;30m" + iteration + "\n");
        System.out.print("\n______________\nIterations: " + iteration + "\n");
        //System.out.println("");
    }

    /**
     * Check if the last column is negative
     *
     * if lastColumn is negative
     *
     *      check if the elements in the row (where the last element is negative (which is constant)
     *      has positive or negative values
     *
     *      if elements in the row are negative
     *          go on with the next iteration
     *
     *      if the elements are non-negative
     *          the problem does not have a possible solution
     *          the algorithm stops
     *
     * For example:
     *      1x1 + 0x2 + 4x2 = -12
     *      Despite the last element is negative (which is the lastColumn in the matrix),
     *      the other values in this row are non-negative, so the problem does
     *      not have a possible solution
     */

    private static boolean hasPositiveValues(int[][] matrix) {
        boolean isPositive = true;
        boolean isNegative = false;

        found:
        for (int i = 0; i < matrix.length - 1; i++) {
            if (isNegative) { break; }
                for (int j = 0; j < matrix[i].length - 1; j++) {

                if (matrix[i][matrix[i].length - 1] < 0) {
                    if (matrix[i][j] < 0) {
                        isPositive = false;
                        break found;
                    }
                    isNegative = true;
                }
            }
        }
        return isPositive;
    }

    /**
     * Check if the "B" vector (which is the last column int the matrix)
     * has a negative value or not
     * If it has negative values:
     *      go on with next iteration
     *      the function return false
     * else
     *      stop -> optimal solution found -> the function returns true
     *
     */

    private static boolean hasPositiveConstants(int[][] matrix) {
        boolean isPositive = true;

        for (int i = 0; i < matrix.length - 1; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][matrix[i].length - 1] < 0) {
                    //System.out.println("Value: " + matrix[i][matrix[i].length - 1]);
                    isPositive = false;
                    break;
                }
            }
        }
        return isPositive;
    }

    /**
     * Enter the number of rows & columns of the matrix
     * Enter the values too
     */

    private static int[][] enterValues() {

        System.out.print("\nEnter the number of rows: ");
        int row = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Enter the number of columns: ");
        int column = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Enter matrix values\n");

        int[][] matrix = new int[row][column];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                matrix[i][j] = scanner.nextInt();
                //System.out.print(" + ");
            }
        }
        return matrix;
    }

    /**
     * Generate random numbers to check if the algorithm works perfectly
     */

    private static int[][] generateMatrix(int rows, int cols) {
        int[][] matrix = new int[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                Random rand = new Random();
                int element = rand.nextInt(6) - 3;
                matrix[i][j] = element;
            }
        }
        return matrix;
    }

    /**
     * Print out the the matrix to the console
     */

    private static void printMatrix(String str, int[][] matrix) {
        System.out.println(str);
        for (int i = 0; i < matrix.length - 1; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (j < matrix[i].length - 2) {
                    if (matrix[i][j] < 0) {
                        System.out.print(" " + matrix[i][j] + "  ");
                    } else {
                        if (matrix[i][j] < 10) {
                            System.out.print("  " + matrix[i][j] + "  ");
                        } else {
                            System.out.print(" " + matrix[i][j] + "  ");
                        }
                    }
                } else if (j < matrix[i].length - 1){
                    if (matrix[i][j] < 0) {
                        System.out.print(" " + matrix[i][j] + "   =  ");
                    } else {
                        System.out.print("  " + matrix[i][j] + "   =  ");
                    }
                } else {
                    if (matrix[i][j] < 0) {
                        System.out.print(" " + matrix[i][j]);
                    } else {
                        System.out.print("  " + matrix[i][j]);
                    }
                }
            }
            System.out.println();
        }

        System.out.println("----------------------------------------");

        for (int i = matrix.length - 1; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (j < matrix[i].length - 2) {
                    if (matrix[i][j] < 0) {
                        System.out.print(" " + matrix[i][j] + "  ");
                    } else {
                        if (matrix[i][j] < 10) {
                            System.out.print("  " + matrix[i][j] + "  ");
                        } else {
                            System.out.print(" " + matrix[i][j] + "  ");
                        }
                    }
                } else if (j < matrix[i].length - 1){
                    if (matrix[i][j] < 0) {
                        System.out.print(" " + matrix[i][j] + "   =  ");
                    } else {
                        System.out.print("  " + matrix[i][j] + "   =  ");
                    }
                } else {
                    if (matrix[i][j] < 0) {
                        System.out.print(" " + matrix[i][j]);
                    } else {
                        System.out.print("  " + matrix[i][j]);
                    }
                }
            }
            System.out.print("   z(min)");
            System.out.println();
        }
    }

    /**
     * Find minimum value in the first row, where the last element is negative
     * (which is the "b" vector)
     */

    private static int findMin(int[][] matrix) {
        int minValue = Integer.MAX_VALUE;
        boolean isFound = false;

        for (int i = 0; i < matrix.length; i++) {

            if (isFound) {
                break;
            }

            for (int j = 0; j < matrix[i].length - 1; j++) {
                if (matrix[i][j] < minValue && matrix[i][matrix[i].length -1] < 0) {
                    //System.out.println("MIN COLUMN: " + matrix[i][matrix[i].length -1]);
                    minValue = matrix[i][j];
                    isFound = true;
                }
            }
        }
        return minValue;
    }

    /**
     * Find minimum value in the first step - get the ROW number of the
     * minimum value in the matrix
     */

    private static int findMinRow(int[][] matrix) {
        int minValue = Integer.MAX_VALUE;
        int minRow = Integer.MAX_VALUE;
        boolean isFound = false;

        for (int i = 0; i < matrix.length; i++) {

            if (isFound) {
                break;
            }

            for (int j = 0; j < matrix[i].length - 1; j++) {
                if (matrix[i][j] < minValue && matrix[i][matrix[i].length -1] < 0) {
                    //System.out.println("MIN COLUMN: " + matrix[i][matrix[i].length -1]);
                    minValue = matrix[i][j];
                    minRow = i;
                    isFound = true;
                }
            }
        }
        return minRow;
    }

    /**
     * Find minimum value in the first step - get the COLUMN number of the
     * minimum value in the matrix
     */

    private static int findMinColumn(int[][] matrix) {
        int minValue = Integer.MAX_VALUE;
        int minColumn = Integer.MAX_VALUE;
        boolean isFound = false;

        for (int i = 0; i < matrix.length; i++) {

            if (isFound) {
                break;
            }

            for (int j = 0; j < matrix[i].length - 1; j++) {
                if (matrix[i][j] < minValue && matrix[i][matrix[i].length -1] < 0) {
                    //System.out.println("MIN COLUMN: " + matrix[i][matrix[i].length -1]);
                    minValue = matrix[i][j];
                    minColumn = j;
                    isFound = true;
                }
            }
        }
        return minColumn;
    }

    /**
     * SIMPLEX ALGORITHM - ITERATION
     */

    private static int[][] nextIteration(int[][] matrix) {

        int[][] newMatrix = matrix;
        int minVal = findMinInNewVector(matrix);
        int column = findMinColumnInNewVector(matrix);
        int row = findMinRowInNewVector(matrix);

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == column && i != row) {
                    newMatrix[i][j] = matrix[i][j] / (minVal * (-1));
                } else if (i == row && j != column) {
                    newMatrix[i][j] = matrix[i][j] / minVal;
                } else if (j != column && i != row && i != (matrix.length - 1)) {
                    newMatrix[i][j] = matrix[i][j] - (matrix[row][j] / matrix[row][column]) * matrix[i][column];
                } else if (i == (matrix.length - 1)) {
                    newMatrix[i][j] = matrix[i][j] - (matrix[row][j]*(-1) / matrix[row][column]) * matrix[i][column];
                } else if (i == row && j == column) {
                    newMatrix[i][j] = 1 / matrix[i][j];
                }
            }
        }
        return newMatrix;
    }


    /**
     * After finding the minimum value, create another matrix
     * which includes an extra vector that the algorithm created after finding the first element
     * thus the new matrix has and extra row
     */

    private static int[][] createNewMatrix(int[][] matrix) {

        int minVal = findMin(matrix);
        int minValRow = findMinRow(matrix);
        int[][] newMatrix = new int[matrix.length + 1][matrix[0].length];

        for (int i = 0; i < newMatrix.length - 1; i++) {
            for (int j = 0; j < newMatrix[i].length; j++) {

                newMatrix[i][j] = matrix[i][j];
                double element;

                if (((double) matrix[minValRow][j] / (minVal * (-1)) < 0)
                   && (((double) matrix[minValRow][j] / (minVal * (-1))) !=
                   matrix[minValRow][j] / (minVal * (-1)))){

                   element = (double) matrix[minValRow][j] / (minVal * (-1));
                   element--;
                } else {
                    element = (double) matrix[minValRow][j] / (minVal * (-1));
                }

                newMatrix[newMatrix.length - 2][j] = (int) element;
                newMatrix[newMatrix.length - 1][j] = matrix[matrix.length - 1][j];
            }
        }
        return newMatrix;
    }


    /**
     *  Find minimum value of the added vector
     */

    private static int findMinInNewVector(int[][] matrix) {

        int maxValue = Integer.MIN_VALUE;
        double highestRatio = Integer.MIN_VALUE;

        for (int i = matrix.length - 2; i < matrix.length - 1; i++) {
            for (int j = 0; j < matrix[i].length - 1; j++) {
                if (matrix[i][j] < 0 && matrix[i + 1][j] >= 0) {
                    if ((double) (matrix[i + 1][j] / matrix[i][j]) > highestRatio) {
                        highestRatio = (double) matrix[i + 1][j] / matrix[i][j];
                        maxValue = matrix[i][j];
                    }
                }
            }
        }
        return maxValue;
    }

    /**
     * Find minimum value of the added vector - get the COLUMN number of the
     * minimum value in the matrix
     */

    private static int findMinRowInNewVector(int[][] matrix) {

        int row = Integer.MIN_VALUE;
        double highestRatio = Integer.MIN_VALUE;

        for (int i = matrix.length - 2; i < matrix.length - 1; i++) {
            for (int j = 0; j < matrix[i].length - 1; j++) {
                if (matrix[i][j] < 0 && matrix[i + 1][j] >= 0) {
                    if ((double) (matrix[i + 1][j] / matrix[i][j]) > highestRatio) {
                        highestRatio = (double) matrix[i + 1][j] / matrix[i][j];
                        row = i;
                    }
                }
            }
        }
        return row;
    }

    /**
     * Find minimum value of the added vector - get the COLUMN number of the
     * minimum value in the matrix
     */

    private static int findMinColumnInNewVector(int[][] matrix) {

        int column = Integer.MIN_VALUE;
        double highestRatio = Integer.MIN_VALUE;

        for (int i = matrix.length - 2; i < matrix.length - 1; i++) {
            for (int j = 0; j < matrix[i].length - 1; j++) {
                if (matrix[i][j] < 0 && matrix[i + 1][j] >= 0) {
                    if ((double) (matrix[i + 1][j] / matrix[i][j]) > highestRatio) {
                        highestRatio = (double) matrix[i + 1][j] / matrix[i][j];
                        column = j;
                    }
                }
            }
        }
        return column;
    }
}
